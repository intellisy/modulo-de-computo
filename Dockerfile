FROM tensorflow/tensorflow

RUN mkdir /opt/api
WORKDIR /opt/api

RUN apt update && apt install -y libsm6 libxext6 libgl1-mesa-glx libsndfile1 ffmpeg gstreamer1.0-plugins-base gstreamer1.0-plugins-ugly

RUN pip install flask gevent requests pillow keras opencv-python scikit-image librosa pygobject

RUN apt-get install -y libxrender-dev

COPY ./api /opt/api