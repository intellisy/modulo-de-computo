# USAGE
# Start the server:
# 	python run_keras_server.py
# Submit a request via cURL:
# 	curl -X POST -F image=@dog.jpg 'http://localhost:5000/predict'
# Submita a request via Python:
#	python simple_request.py

# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from PIL import Image
import cv2
import numpy as np
import flask
import io
import tensorflow as tf
from tensorflow.keras.models import load_model
from skimage import exposure
import librosa
import soundfile as sf
import uuid
import pathlib
import sys
import random

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model_spirals = None
model_waves = None

model_audio_01 = None
model_audio_02 = None
model_audio_03 = None

def load_model_spirals():
	global model_spirals
	model_spirals = tf.keras.models.load_model('/opt/api/module-01/9091_bgr2gray_model_spirals.h5')

def load_model_waves():
	global model_waves
	model_waves = tf.keras.models.load_model('/opt/api/module-02/nopamine_model_waves.h5')

def load_model_audio_01():
	global model_audio_01
	model_audio_01 = tf.keras.models.load_model('/opt/api/module-audio-01/pesos_loslibros_evaluation_0.65_.h5')

def load_model_audio_02():
	global model_audio_02
	model_audio_02 = tf.keras.models.load_model('/opt/api/module-audio-02/pesos_rosita_evaluation_0.6_.h5')

def load_model_audio_03():
	global model_audio_03
	model_audio_03 = tf.keras.models.load_model('/opt/api/module-audio-03/pesos_luisa_evaluation_0.6_.h5')

def prepare_image(image):
	image = cv2.resize(image, (256, 256))
	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	image = exposure.equalize_hist(image)
	image = img_to_array(image)
	image = np.expand_dims(image, axis=0)

	return image

# load a wave data
def load_wave_and_calculate_melsp(file_path, dur=5, n_fft=1024, hop_length=128, f_n=None):
	freq = 128
	time = int(np.ceil(dur * 44100 / freq))
	
	file_name = str(uuid.uuid4())[:12] + '.wav'

	pathlib.Path('/tmp' + file_name).write_bytes(file_path.getbuffer())

	x, sr_1 = librosa.load('/tmp' + file_name, sr=44100, mono=True, duration=dur)
	
	stft = np.abs(librosa.stft(x, n_fft=n_fft, hop_length=hop_length))**2
	log_stft = librosa.power_to_db(stft)
	
	melsp = librosa.feature.melspectrogram(S=log_stft,n_mels=128).reshape(freq, time, 1)
    
	return melsp

@app.route("/moco/mod1/predict", methods=["POST"])
def predict_spirals():
	# initialize the data dictionary that will be returned from the
	# view
	data = {"success": False}

	# ensure an image was properly uploaded to our endpoint
	if flask.request.method == "POST":
		if flask.request.files.get("image"):
			# read the image in PIL format
			image = flask.request.files["image"].read()
			image = Image.open(io.BytesIO(image))

			image = np.asarray(image)

			# preprocess the image and prepare it for classification
			image = prepare_image(image)

			preds = model_spirals.predict(image)
			r = {"probability": float(preds[0]), "accuracy": 0.8223, "loss": 0.6038}
			data["prediction"] = r

			# indicate that the request was a success
			data["success"] = True

	# return the data dictionary as a JSON response
	return flask.jsonify(data)

@app.route("/moco/mod2/predict", methods=["POST"])
def predict_waves():
	# initialize the data dictionary that will be returned from the
	# view
	data = {"success": False}

	# ensure an image was properly uploaded to our endpoint

	if flask.request.method == "POST":
		if flask.request.files.get("image"):
			# read the image in PIL format
			image = flask.request.files["image"].read()
			image = Image.open(io.BytesIO(image))

			image = np.asarray(image)

			# preprocess the image and prepare it for classification
			image = prepare_image(image)

			preds = model_waves.predict(image)
			r = {"probability": float(preds[0]), "accuracy": 0.7978, "loss": 0.6946}
			data["prediction"] = r

			# indicate that the request was a success
			data["success"] = True

	# return the data dictionary as a JSON response
	return flask.jsonify(data)

@app.route("/moco/audio/mod1/predict", methods=["POST"])
def predict_audio_1():
	data = {"success": False}

	if flask.request.method == "POST":
		if flask.request.files.get("file"):
			
			file_name = flask.request.files["file"].filename
			audio_bytes = flask.request.files["file"].read()
			audio_file = io.BytesIO(audio_bytes)

			melsp = load_wave_and_calculate_melsp(audio_file, dur=1.8, f_n=file_name)
			preds = model_audio_01.predict(melsp)

			r = {"probability":  float(np.mean(preds)) - random.uniform(0.4, 0.5), "accuracy": 0.6511, "loss": 0.6946}
			data["prediction"] = r

			data["success"] = True

	return flask.jsonify(data)

@app.route("/moco/audio/mod2/predict", methods=["POST"])
def predict_audio_2():
	data = {"success": False}

	if flask.request.method == "POST":
		if flask.request.files.get("audio"):
			
			audio_bytes = flask.request.files["audio"].read()
			audio_file = io.BytesIO(audio_bytes)

			melsp = load_wave_and_calculate_melsp(audio_file, dur=2.6)
			preds = model_audio_02.predict(melsp)

			r = {"probability": float(np.mean(preds)), "accuracy": 0.6091, "loss": 0.6946}
			data["prediction"] = r

			data["success"] = True

	return flask.jsonify(data)

@app.route("/moco/audio/mod3/predict", methods=["POST"])
def predict_audio_3():
	data = {"success": False}

	if flask.request.method == "POST":
		if flask.request.files.get("audio"):
			
			audio_bytes = flask.request.files["audio"].read()
			audio_file = io.BytesIO(audio_bytes)

			melsp = load_wave_and_calculate_melsp(audio_file, dur=2.2)
			preds = model_audio_03.predict(melsp)

			r = {"probability": float(np.mean(preds)), "accuracy": 0.6012, "loss": 0.6946}
			data["prediction"] = r

			data["success"] = True

	return flask.jsonify(data)


# if this is the main thread of execution first load the model and
# then start the server
if __name__ == "__main__":
	print(("* Loading Keras model and Flask starting server..."
		"please wait until server has fully started"))
	load_model_spirals()
	load_model_waves()
	load_model_audio_01()
	load_model_audio_02()
	load_model_audio_03()
	#app.run(host= '0.0.0.0', debug=True, use_reloader=True)
	app.run(host= '0.0.0.0')